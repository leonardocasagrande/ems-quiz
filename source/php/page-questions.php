<div class="questions">   

    <div class="hits animated slideInLeft delay-0s5m">

        <img src="<?=get_template_directory_uri();?>/dist/img/hits.png" class="animated slideInDown delay-1s">

        <span class="animated fadeIn delay-1s">
            00
        </span>     

    </div>

    <div class="errors animated slideInRight delay-0s5m">

        <img src="<?=get_template_directory_uri();?>/dist/img/errors.png" class="animated slideInDown delay-1s">

        <span class="animated fadeIn delay-1s">
            00
        </span>     

    </div>

    <a href="#/">
        <img src="<?=get_template_directory_uri();?>/dist/img/logo-ems-quiz.png" alt="" class="logo-ems-quiz animated fadeIn delay-0s5m">
    </a>

    <img src="<?=get_template_directory_uri();?>/dist/img/obrigado.png" alt="" class="obrigado-img d-none">

    

    <div class="tab animated slideInUp delay-1s">

        <div class="question animated fadeIn delay-1s5m"></div>
        <div class="alternatives"></div>
    </div>
    <div class="time-bar animated fadeIn delay-1s5m">
        <div class="bar"></div>
    </div>
    <div class="choice-callback d-none">
        <img src="<?=get_template_directory_uri();?>/dist/img/acertou.png" alt="" class="acertou-img d-none">
        <img src="<?=get_template_directory_uri();?>/dist/img/errou-img.png" alt="" class="errou-img d-none">
    </div>

</div>