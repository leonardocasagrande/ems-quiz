<div class="form">   

    <img src="<?=get_template_directory_uri();?>/dist/img/logo-ems-quiz.png" alt="" class="logo-ems-quiz animated fadeIn delay-0s5m">
    
    <div class="title"><span class="quiz-name"></span> Quiz</div>

    <div class="sub-title">
        <strong>Teste seus conhecimentos sobre <span class="quiz-name"></span> e suas indicações!</strong><br/>
        Para iniciar, insira seu nome e email.
    </div>
    <div class="lead-form">

        <div class="step-1">
            <input type="text" placeholder="Número do Setor" id="input-sector" autocomplete="off" required>
            <input type="text" placeholder="Rede e Nome da Loja" id="input-store" autocomplete="off" required>
        </div>

        <div class="step-2 d-none">
            <input type="text" placeholder="Nome Balconista/Farmacêutico" id="input-name" autocomplete="off" required>
            <input type="email" placeholder="E-mail" id="input-email" autocomplete="off" required>
        </div>
        
                
    </div>

    <img src="<?=get_template_directory_uri();?>/dist/img/btn-next.png" alt="" class="bt-next-step animated fadeIn delay-0s5m">
    
    <img src="<?=get_template_directory_uri();?>/dist/img/bt-iniciar.png" alt="" class="bt-send d-none">

</div>