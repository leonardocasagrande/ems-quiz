<?php 
function create_post_type(){

    register_post_type('quiz',
    array(
        'labels' => array(
            'name' => __('Quiz'),
            'singular_name' => __('Quiz')
        ),
        'public' => true,
        'has_archive' => false,
        'show_in_rest' => true,
        'menu_icon'   => 'dashicons-format-status'
    ));

}

add_action('init', 'create_post_type');

// Enable the option show in rest
add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );

// Enable the option edit in rest
add_filter( 'acf/rest_api/field_settings/edit_in_rest', '__return_true' );

add_action('admin_menu', 'remove_posts_menu');
function remove_posts_menu(){
    remove_menu_page('edit.php');
}
?>