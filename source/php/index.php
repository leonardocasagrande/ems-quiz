<?php
$VER = "v1.7.1";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>EMS Quiz</title>
  <meta name="codelab" content="your-first-pwa-v3">
  <link rel="manifest" href="<?=site_url();?>/manifest.json">
  <link rel='stylesheet' href='<?=get_template_directory_uri();?>/dist/css/style.css?v<?=$VER?>'>
</head>
<body>
<div class="app"></div>
<script src="<?=get_template_directory_uri();?>/dist/js/app.js?v<?=$VER?>"></script>
</body>
</html>