var siteURL = "https://linha-vital.humannhealth.com.br/";
//var siteURL = "http://emsquiz.io/";

var apiURL = siteURL+ "wp-json/wp/v2/";

var routes = {},
    defaultRoute = 'starting';

routes['starting'] = {
    url: '#/',
    templateUrl: siteURL + 'starting/'
};

routes['form'] = {
    url: '#/form',
    templateUrl: siteURL + 'form/'
};

routes['questions'] = {
    url: '#/questions',
    templateUrl: siteURL + 'questions/'
};

routes['config'] = {
    url: '#/config',
    templateUrl: siteURL + 'config/'
};

$.router
    .setData(routes)
    .setDefault(defaultRoute);

$.when($.ready)
    .then(function() {
        $.router.run('.app', 'starting');
    });

if (typeof(Storage) !== "undefined") {
    //alert('Storage');
} else {
    alert('Not support Storage');
}

$.router.onRouteChanged( function(e, route, params){

    switch (route.name) {
        case 'form':
            FormScript();
            break;
        case 'starting':
            StartingScript();
            break;
        case 'questions':
            QuestionsScript();
            break;
        case 'config':
            ConfigScript();
            break;
        default:
            console.log('Sorry, we are out of .');
        }
});

var StartingScript = function(){
    var today = new Date();
    var tomorrow = new Date();
    tomorrow.setDate(today.getDate()+1);
    today.setHours(0,0,0,0);
    tomorrow.setHours(0,0,0,0);
    var nextDay = localStorage.getItem('nextDay');
    if (nextDay == null) {
        localStorage.setItem('nextDay',tomorrow)
        nextDay = tomorrow
    }else{
        nextDay = new Date(nextDay)
    } 
    if(nextDay.getTime() <= today.getTime()) {
        
        var leadsPost = localStorage.getItem('leads');
        $.ajax({
            url: siteURL + "lead-send/",
            type: "POST",
            data: { data: JSON.parse(leadsPost) },
            dataType: "text"
        }).done(function(data) {
            
            localStorage.removeItem('leads');

            alert("Sincronizado com Sucesso");

        }).fail(function(jqXHR, textStatus ) {
    
        }).always(function() {
    
            // console.log("completou");
    
        });
        localStorage.removeItem('leads');
        localStorage.removeItem('nextDay');
        localStorage.setItem('nextDay',tomorrow);
    }
    
    QuizLocalStorage();
}

var ConfigScript = function (){   

    $(".btn-sync").click(function() {

        var leadsPost = localStorage.getItem('leads');

        if(leadsPost != null){

            $.ajax({
                url: siteURL + "lead-send/",
                type: "POST",
                data: { data: JSON.parse(leadsPost) },
                dataType: "text"
            }).done(function(data) {
                
                localStorage.removeItem('leads');

                alert("Sincronizado com Sucesso");
    
            }).fail(function(jqXHR, textStatus ) {
        
            }).always(function() {
        
                // console.log("completou");
        
            });

        }        

        localStorage.removeItem('quiz');

        QuizLocalStorage();     
        
        $.router.go("starting");

    });

    var elmOn = $(".status-on");

    var elmOff = $(".status-off");

    var btnSync = $(".btn-sync");

    $.ajax({
        url: apiURL + "quiz",
        type: "GET",
        dataType: "json"
    }).done(function(data) {

        elmOn.removeClass("d-none");

        btnSync.removeClass("d-none");

        elmOff.addClass("d-none");

        alert("Perguntas Sincronizadas");

    }).fail(function(jqXHR, textStatus ) {

        elmOff.removeClass("d-none");

        btnSync.addClass("d-none");

        elmOn.addClass("d-none");

    }).always(function() {

        // console.log("completou");

    });

}

var QuestionsScript = function (){

    localStorage.removeItem('endGame');

    var quiz = QuizLocalStorage();

    ShowQuestion(0, quiz.acf.questions);

    $(".questions").on("click", '.btn-choice', function() {

        var isCorrect = $(this).data('correct');

        var questionIndex = $(this).data('questionIndex');

        var callBackImgClass = "animated tada";

        var elmCallBack = $(".choice-callback");

        var elmImg;

        var timeoutProgress;

        elmCallBack.removeClass("d-none");

        if(isCorrect){

            var elm = $(".hits span");

            var hits  = parseInt(elm.text());

            elmImg = $(".acertou-img");				

            elm.html((hits+1).pad());							

        }else{

            var elm = $(".errors span");

            var errors  = parseInt(elm.text());

            elmImg = $(".errou-img");	

            elm.html((errors+1).pad());

        }

        elmImg.removeClass('d-none').addClass(callBackImgClass);	

        setTimeout(function(){

            elmCallBack.addClass('d-none');

            elmImg.addClass('d-none').removeClass(callBackImgClass);

        }, 1100);

        var nextQuestion = questionIndex + 1;

        if(nextQuestion >= quiz.acf.questions.length){

            var timeLeft = $(".bar").data('time');

            EndGame(timeLeft);

        }else{

            ShowQuestion(nextQuestion, quiz.acf.questions);		

        }				

    });

    

    function progress(timeleft, timetotal, $element) {

        var progressBarWidth = timeleft * $element.width() / timetotal;

        $element.find('div').animate({ width: progressBarWidth }, 500).html((Math.floor(timeleft/60)).pad() + ":"+ (timeleft%60).pad());

        $(".bar").attr('data-time', timeleft);

        if(timeleft > 0) {            

            timeoutProgress = setTimeout(function() {

                progress(timeleft - 1, timetotal, $element);

            }, 1000);

            if(localStorage.getItem('endGame')){

                clearInterval(timeoutProgress);

            }            

        }else{				

            EndGame(timeleft);

        }
    };

    var timer = quiz.acf.time * 60;	

    progress(timer, timer, $('.time-bar'));



}

var EndGame = function(timeLeft){

    localStorage.setItem('endGame', true);
    
    var leads = JSON.parse(localStorage.getItem('leads'));

    var lastLead = leads[leads.length - 1];

    var elmHits = $(".hits span");

    var hits  = parseInt(elmHits.text());

    var elmErrors = $(".errors span");

    var errors  = parseInt(elmErrors.text());

    var lead = {
        name:lastLead.name,
        email:lastLead.email,
        sector:lastLead.sector,
        store:lastLead.store,
        hits:hits, 
        errors:errors,
        time:timeLeft
    };

    
    leads[leads.length - 1] = lead;
    
    localStorage.setItem('leads', JSON.stringify(leads));

    $(".tab").html(" ");

    $(".time-bar").remove();

    var elmObrigado = $(".obrigado-img");

    elmObrigado.removeClass("d-none");

    elmObrigado.click(function() {

        elmObrigado.addClass("d-none");

        $(".tab").html('<div class="list-leads-title">Ranking</div><div class="list-leads"><a href="#/"><img class="botao-voltar" src="'+siteURL+'wp-content/themes/quiz-theme/dist/img/botao-voltar.png"></a></div>');

        var quiz = QuizLocalStorage();    
        
        leads = leads.sort(function(a, b) {
            return parseFloat(a.hits) - parseFloat(b.hits);
        });

        $.each(leads, function(i, item) {	

            var timeFinesh = (quiz.acf.time * 60) - item.time;

            $(".list-leads").prepend('<div>'+ item.name + ' - '+ item.hits + ' Acerto(s) e '+ item.errors + ' Erro(s) - Tempo: '+ FormatSeconds(timeFinesh) + '</div>');
    
        });

    });    

}

var ShowQuestion = function(index, questions){

    var animateDelay = "delay-2s";

    if(index > 0){

        animateDelay = "delay-0s5m";

    }

    var questionNumber = index + 1;

    $(".question").html((questionNumber).pad() + " - " + questions[index].question);

    var choices = ["A", "B", "C", "D"];	

    $(".alternatives").html("");

    $.each(questions[index].alternatives, function(i, item) {			

        $(".alternatives").append('<button class="btn-choice ' + animateDelay + '" data-correct="' + item.correct + '" data-question-index="' + index + '"><div class="choice"><span>' + choices[i] + '</span></div><div class="alternative">' + item.answers + '</div></button>');

    });		

}



var QuizLocalStorage = function (){

    var quizLocalStorage = JSON.parse(localStorage.getItem('quiz'));

    if(quizLocalStorage === null){

        $.ajax({
            url: apiURL + "quiz",
            type: "GET",
            dataType: "json"
        }).done(function(data) {

            localStorage.setItem('quiz', JSON.stringify(data));

            quizLocalStorage = data;

        }).fail(function(jqXHR, textStatus ) {

            console.log("Request failed: " + textStatus);

        }).always(function() {

            // console.log("completou");

        });

    }

    $(".quiz-name").html(quizLocalStorage[0].title.rendered);		
    
    return quizLocalStorage[0];

}



var FormScript = function (){
    
    QuizLocalStorage();

    var elm_sector = $("#input-sector");

    var elm_store = $("#input-store");

    
    $(".bt-next-step").click(function() {
        console.log(elm_store.val());
    console.log(elm_sector.val());
        if(validateNome(elm_store.val()) && elm_store.val().length > 3 && elm_sector.val().length > 0 && validateSetor(elm_sector.val())){

            $(".step-1, .bt-next-step").addClass("d-none");
            $(".step-2, .bt-send").removeClass("d-none");


        }else{

            alert("Número do Setor ou Rede Inválidos");

        }

    });

    $(".bt-send").click(function() {

        var leads = [];

        var leadsLocalStorage = JSON.parse(localStorage.getItem('leads'));

        if(leadsLocalStorage != null){

            leads = JSON.parse(localStorage.getItem('leads'));

        }

        var name = $("#input-name").val();

        var email = $("#input-email").val();

        var sector = elm_sector.val();

        var store = elm_store.val();

        if(!validateEmail(email) && !validateNome(name)){

            alert("E-mail Inválido ou Nome Inválido");

        }else{

            var lead = {
                name:name,
                email:email,
                sector:sector,
                store:store,
                hits:0, 
                errors:0,
                time:0
            };

            leads.push(lead);

            localStorage.setItem('leads', JSON.stringify(leads));

            $.router.go("questions");

        }


    });
}
var FormatSeconds = function (time) {   
    // Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~time % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";
    if (hrs > 0) {
        ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    }
    ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
}

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

var validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
var validateNome = function(nome){
    var reNome = /^[a-zA-Z\s]*$/;
    return reNome.test(nome);
}
var validateSetor = function(setor){
    var reSetor =/^[0-9]*$/;
    return reSetor.test(setor);
}