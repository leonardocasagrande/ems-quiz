importScripts('/cache-polyfill.js');


self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('emsquiz_kolid').then(function(cache) {
     return cache.addAll([
       '/emsquiz_kolid/starting/',
       '/emsquiz_kolid/form/',
       '/emsquiz_kolid/questions/',
       '/emsquiz_kolid/config/',
       '/emsquiz_kolid/wp-content/themes/quiz-theme/dist/css/style.css',
       '/emsquiz_kolid/wp-content/themes/quiz-theme/dist/js/app.js'
     ]);
   })
 );
});